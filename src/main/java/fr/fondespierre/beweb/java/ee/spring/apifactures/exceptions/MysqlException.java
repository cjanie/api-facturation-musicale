package fr.fondespierre.beweb.java.ee.spring.apifactures.exceptions;

public class MysqlException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MysqlException() {
		super("Bill web service error: Mysql server error");
	}

}
