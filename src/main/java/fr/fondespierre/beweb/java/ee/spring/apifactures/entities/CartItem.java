package fr.fondespierre.beweb.java.ee.spring.apifactures.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class CartItem {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "item")
	private Item item;
	
	@Column
	private Long itemId;
	
	@Column
	private String itemDesignation;
	
	@Column
	private Float unityPrice;
	
	@Column
	private Integer quantity;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "cart")
	@JsonIgnore
	private Cart cart;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Item getItem() {
		return item;
	}
	
	

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	public void setItem(Item item) {
		this.item = item;
	}
	
	

	public String getItemDesignation() {
		return itemDesignation;
	}

	public void setItemDesignation(String itemDesignation) {
		this.itemDesignation = itemDesignation;
	}

	public Float getUnityPrice() {
		return unityPrice;
	}

	public void setUnityPrice(Float unityPrice) {
		this.unityPrice = unityPrice;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}
	
	
	

}
