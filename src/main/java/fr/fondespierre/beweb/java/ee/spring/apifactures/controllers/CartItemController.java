package fr.fondespierre.beweb.java.ee.spring.apifactures.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.fondespierre.beweb.java.ee.spring.apifactures.entities.CartItem;
import fr.fondespierre.beweb.java.ee.spring.apifactures.exceptions.MysqlException;
import fr.fondespierre.beweb.java.ee.spring.apifactures.services.CartItemService;

@RestController
@RequestMapping(value = "/rest/cart-items")
@CrossOrigin(origins = "*")
public class CartItemController {
	
	@Autowired
	private CartItemService cartItemService;
	
	@GetMapping
	public ResponseEntity<?> listCartItems() {
		List<CartItem> cartItems = null;
		try {
			cartItems = this.cartItemService.list();
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<CartItem>>(cartItems, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getCartItem(@PathVariable Long id) {
		CartItem cartItem = null;
		try {
			cartItem = this.cartItemService.get(id);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<CartItem>(cartItem, HttpStatus.OK);
	}
	
	


}
