package fr.fondespierre.beweb.java.ee.spring.apifactures.repositories;

import org.springframework.data.repository.CrudRepository;

import fr.fondespierre.beweb.java.ee.spring.apifactures.entities.Company;

public interface CompanyRepository extends CrudRepository<Company, Long>{

}
