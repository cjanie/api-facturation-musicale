package fr.fondespierre.beweb.java.ee.spring.apifactures.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.fondespierre.beweb.java.ee.spring.apifactures.entities.Company;
import fr.fondespierre.beweb.java.ee.spring.apifactures.exceptions.MysqlException;
import fr.fondespierre.beweb.java.ee.spring.apifactures.services.CompanyService;

@RestController
@RequestMapping(value = "/rest/companies")
@CrossOrigin(origins = "*")
public class CompanyController {
	
	@Autowired
	private CompanyService companyService;
	
	@GetMapping
	public ResponseEntity<?> listCompanies() {
		List<Company> companies = null;
		try {
			companies = this.companyService.list();
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Company>>(companies, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getCompany(@PathVariable Long id) {
		Company company = null;
		try {
			company = this.companyService.get(id);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Company>(company, HttpStatus.OK);
	}

}
