package fr.fondespierre.beweb.java.ee.spring.apifactures.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.fondespierre.beweb.java.ee.spring.apifactures.entities.Bill;
import fr.fondespierre.beweb.java.ee.spring.apifactures.entities.CartItem;
import fr.fondespierre.beweb.java.ee.spring.apifactures.exceptions.MysqlException;
import fr.fondespierre.beweb.java.ee.spring.apifactures.repositories.BillRepository;

@Service
public class BillService {
	
	@Autowired
	private BillRepository billRepository;
	@Autowired
	private CartItemService cartItemService;
	
	public List<Bill> list() throws MysqlException {
		List<Bill> bills = new ArrayList<>();
		this.billRepository.findAll().forEach(bill -> {
			bills.add(bill);
		});
		return bills;
	}
	
	public Bill get(Long id) throws MysqlException {
		return this.billRepository.findById(id).get();
	}
	
	public Long add(Bill bill) throws MysqlException {
		this.billRepository.save(bill);
		for(int i=0; i<bill.getCart().getCartItems().size(); i++) {
			CartItem cartItem = new CartItem();
			cartItem.setCart(bill.getCart());
			cartItem.setItem(bill.getCart().getCartItems().get(i).getItem());
			cartItem.setItemId(bill.getCart().getCartItems().get(i).getItemId());
			cartItem.setItemDesignation(bill.getCart().getCartItems().get(i).getItemDesignation());
			cartItem.setUnityPrice(bill.getCart().getCartItems().get(i).getUnityPrice());
			cartItem.setQuantity(bill.getCart().getCartItems().get(i).getQuantity());
			this.cartItemService.add(cartItem);
		}
		return bill.getId();
	}
	
	public void update(Bill bill) throws MysqlException {
		this.billRepository.save(bill);
	}
	
	public void delete(Long id) throws MysqlException {
		this.billRepository.deleteById(id);
	}
}
