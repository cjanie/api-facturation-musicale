package fr.fondespierre.beweb.java.ee.spring.apifactures.repositories;

import org.springframework.data.repository.CrudRepository;

import fr.fondespierre.beweb.java.ee.spring.apifactures.entities.Bill;

public interface BillRepository extends CrudRepository <Bill, Long> {

}
