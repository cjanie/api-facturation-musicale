package fr.fondespierre.beweb.java.ee.spring.apifactures.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.fondespierre.beweb.java.ee.spring.apifactures.entities.Bill;
import fr.fondespierre.beweb.java.ee.spring.apifactures.exceptions.MysqlException;
import fr.fondespierre.beweb.java.ee.spring.apifactures.services.BillService;

@RestController
@RequestMapping(value = "/rest/bills")
@CrossOrigin(origins = "*")
public class BillController {

	@Autowired
	private BillService billService;
	
	@GetMapping("")
	public ResponseEntity<?> listBills() {
		List<Bill> bills = null; 
		try {
			bills = this.billService.list();
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Bill>>(bills, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getBill(@PathVariable Long id) {
		Bill bill = null;
		try {
			bill = this.billService.get(id);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Bill>(bill, HttpStatus.OK);
	}
	
	@PostMapping("")
	public ResponseEntity<?> addBill(@RequestBody Bill bill) {
		Long id = null;
		try {
			id = this.billService.add(bill);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(id, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateBill(@RequestBody Bill bill) {
		try {
			this.billService.update(bill);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteBill(@PathVariable Long id) {
		try {
			this.billService.delete(id);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
}
