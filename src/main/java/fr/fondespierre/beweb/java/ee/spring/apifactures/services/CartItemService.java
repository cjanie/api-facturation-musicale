package fr.fondespierre.beweb.java.ee.spring.apifactures.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.fondespierre.beweb.java.ee.spring.apifactures.entities.CartItem;
import fr.fondespierre.beweb.java.ee.spring.apifactures.exceptions.MysqlException;
import fr.fondespierre.beweb.java.ee.spring.apifactures.repositories.CartItemRepository;

@Service
public class CartItemService {

	@Autowired
	private CartItemRepository cartItemRepository;
	
	public List<CartItem> list() throws MysqlException {
		List<CartItem> cartItems = new ArrayList<>();
		this.cartItemRepository.findAll().forEach(cartItem -> {
			cartItems.add(cartItem);
		});
		return cartItems;
	}
	
	public CartItem get(Long id) throws MysqlException {
		return this.cartItemRepository.findById(id).get();
	}
	
	public Long add(CartItem cartItem) throws MysqlException {
		return this.cartItemRepository.save(cartItem).getId();
	}
	
	public void update(CartItem cartItem) throws MysqlException {
		this.cartItemRepository.save(cartItem);
	}
	
	public void delete(Long id) throws MysqlException {
		this.cartItemRepository.deleteById(id);
	}
}
