package fr.fondespierre.beweb.java.ee.spring.apifactures.enums;

public enum BillStatus {

	PAYED, UNPAYED
}
