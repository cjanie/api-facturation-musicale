package fr.fondespierre.beweb.java.ee.spring.apifactures.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.fondespierre.beweb.java.ee.spring.apifactures.entities.Company;
import fr.fondespierre.beweb.java.ee.spring.apifactures.exceptions.MysqlException;
import fr.fondespierre.beweb.java.ee.spring.apifactures.repositories.CompanyRepository;

@Service
public class CompanyService {
	
	@Autowired
	private CompanyRepository companyRepository;
	
	public List<Company> list() throws MysqlException {
		List<Company> companies = new ArrayList<>();
		this.companyRepository.findAll().forEach(company -> {
			companies.add(company);
		});
		return companies;
	}
	
	public Company get(Long id) throws MysqlException {
		return this.companyRepository.findById(id).get();
	}

}
