package fr.fondespierre.beweb.java.ee.spring.apifactures.enums;

public enum PaymentMethod {
	CASH, CHECK, CARD

}
