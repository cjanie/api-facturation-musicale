package fr.fondespierre.beweb.java.ee.spring.apifactures;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApifacturesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApifacturesApplication.class, args);
	}

}
